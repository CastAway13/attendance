package fi.vamk.e1900307.test.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class DemoController {

	/*@GetMapping("/")
	public String swagger() {
		return "redirect:/swagger-ui.html";
	}*/

	@RequestMapping("/test")
	public String test() {
		return "{\"id\":1}";
	}
}
